#!/usr/bin/python3

'''
COBS ENCODING, DECODING
AVR COMPATIBLE CRC16 FUNCTIONS

Author: Anders Draagen (andersdra@gmail.com)
'''

def crc16_update(crc, byte):
    crc ^= byte
    for a in range(8):
        if (crc & 0x1):
            crc = (crc >> 1) ^ 0x8408
        else:
            crc = (crc >> 1)
    return crc

def crc16_arr(bytelist):
    crc = 0xffff
    for a in range(len(bytelist)):
        crc ^= bytelist[a]
        for b in range(8):
            if (crc & 0x1):
                crc = (crc >> 1) ^ 0x8408
            else:
                crc = (crc >> 1)
    return crc

def testcobs():
    print(cobs_encode([b'\xff', '00', '0xaa', 199]))

'''
replaces all zero bytes in input with places until the next occurrence
first byte is places until first encoded zero
first actual zero encountered in returndata is end of encoded data
input bytearray or a list of bytes
list can consist of integer, hex(directly or as string 'ff', '0xff') \
byte or bytearray(of a single byte!), mixed as much as you desire

EXCEPTIONS:
ValueError will be raised if trying to encode something > 255

EXAMPLES:
>>> cobs_encode(b'\x11\x22\x00\x33')
bytearray(b'\x03\x11"\x023\x00')
>>> cobs_encode([b'\xff', '00', 0x0, '0xaa', 0xaa, 199])
bytearray(b'\x02\xff\x01\x04\xaa\xaa\xc7\x00')
>>> cobs_encode([1, 2, 3, 0, 4, 0, 5])
bytearray(b'\x04\x01\x02\x03\x02\x04\x02\x05\x00')
'''
def cobs_encode(inbytes):
    encode_me = bytearray()
    try: # copy input to bytearray
        encode_me = bytearray(inbytes[:])
    except ValueError: # something > 255 input
        raise
    except TypeError: # string at input
        for count, data in enumerate(inbytes):
            try: # append hexes as integers
                encode_me.append(int(inbytes[count], 16))
            except TypeError: # integer in mixed input
                encode_me.append(inbytes[count])
            except ValueError: # byte
                encode_me.append(int(inbytes[count].hex(), 16))
    encode_me.append(0x0) # endbyte
    encoded_data = encode_me[:] # enumerating encode_me and editing at the same time, nono..
    prev = 0 # keep track of where first/previous zero was encountered
    if inbytes:
        for count, data in enumerate(encode_me):
            if data == 0x0:
                if prev: # already encountered a zero in data
                    encoded_data[prev] = count-(prev-1) # replace previous zero with places until the one encountered now
                else: # first zero encountered, either in data or the one appended as endbyte
                    encoded_data[0:0] = [(count+1)] # prepend encoded bytes with places until first zero
                prev = (count+1) # add one, or it will fail if fed [0x0] or [0x0, 0x2, 0x3] etc
    else: # no input
        encoded_data[0:0] = [0x1] # prepend a 1 to appended zero to make a valid no data COBS packet
    return encoded_data


def cobs_crc_encode(inbytes):
    crc = 0xffff
    encode_me = bytearray()
    try: # copy input to bytearray
        encode_me = bytearray(inbytes[:])
    except ValueError: # something > 255 input
        raise
    except TypeError: # string at input
        for count, data in enumerate(inbytes):
            try: # append hexes as integers
                encode_me.append(int(inbytes[count], 16))
            except TypeError: # integer in mixed input
                encode_me.append(inbytes[count])
            except ValueError: # byte
                encode_me.append(int(inbytes[count].hex(), 16))
    crc = crc16_arr(encode_me)
    encode_me.append((crc & 0xff00) >> 8)
    encode_me.append(crc & 0xff)
    encode_me.append(0x0) # endbyte
    encoded_data = encode_me[:] # enumerating encode_me and editing at the same time, nono..
    prev = 0 # keep track of where first/previous zero was encountered
    if inbytes:
        for count, data in enumerate(encode_me):
            if data == 0x0:
                if prev: # already encountered a zero in data
                    encoded_data[prev] = count-(prev-1) # replace previous zero with places until the one encountered now
                else: # first zero encountered, either in data or the one appended as endbyte
                    encoded_data[0:0] = [(count+1)] # prepend encoded bytes with places until first zero
                prev = (count+1) # add one, or it will fail if fed [0x0] or [0x0, 0x2, 0x3] etc
    else: # no input
        encoded_data[0:0] = [0x1] # prepend a 1 to appended zero to make a valid no data COBS packet
    return encoded_data


def cobs_decode(inlist):
    decode = bytearray(inlist[:])
    nextzero = inlist[0]
    while True:
        if decode[nextzero] == 0x0:
            break
        nextup = nextzero+decode[nextzero]
        decode[nextzero] = 0x0
        nextzero = nextup
    return decode[1:-1]
